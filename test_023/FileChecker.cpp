#include "FileChecker.h"

#include <QElapsedTimer>
// Choose and decoment THE ONLY ONE
// var1
int FileChecker::cPageSize = 0x100000;
int FileChecker::cPageCount = 4;
// var2
//int FileChecker::cPageSize = 0x10000;
//int FileChecker::cPageCount = 64;

FileChecker::FileChecker(const FSViewer *_fsv)
    : matcher(new QByteArrayMatcher())
{
    fsv = const_cast<FSViewer*>(_fsv);
    matcher->setPattern(fsv->matchPattern());
    patternSize = matcher->pattern().length();
}

FileChecker::~FileChecker()
{
    cleanup();
}

void FileChecker::startCheck(const QString &_fileName, qint64 _fileSize, int _fileKey)
{
    fileName = _fileName;
    fileKey = _fileKey;
    fileSize = _fileSize;
    size = _fileSize; // currently effective size: decrease while reading
    connect(this, SIGNAL(matchFound(int,int)), fsv->tblModel, SLOT(incCol3AsInt(int,int)));
    connect(this, SIGNAL(checkDone(int,double)), fsv->tblModel, SLOT(setCol4AsDbl(int,double)));
}

QByteArray FileChecker::readNextPage(QFile &file, bool &haveTail)
{
    haveTail = (size > cPageSize);
    int chunkSize;
    if (haveTail) {
        chunkSize = cPageSize;
        size -= chunkSize;
    }
    else {
        chunkSize = size;
        size = 0;
    }
    return file.read(chunkSize);
}

void FileChecker::cleanup()
{
    this->disconnect();
}

int FileChecker::countMatches(const QByteArray &ba, int from, int to)
{
    int offset = from;
    int foundCount = 0;
    while(true) {
        int matchPos = matcher->indexIn(ba, offset);
        if ((matchPos == -1) || (matchPos >= to)) break;
        foundCount++;
        offset = matchPos + patternSize;
    }
    return foundCount;
}

void FileChecker::run()
{
    QElapsedTimer timer;
    timer.start();
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Fail to open " << fileName << ":" << file.errorString();
        emit checkDone(fileKey, 1.0e-9l * timer.nsecsElapsed());
        cleanup();
        return;
    }
    bool haveMoreData = true;
    while (pageList.count() < cPageCount) {
        pageList.append(readNextPage(file, haveMoreData));
        if (!haveMoreData) break;
    }
    while (haveMoreData) {
        QByteArrayList tail;
        tail.append(pageList[pageList.count() - 1]);
        QByteArray head(pageList.join());
        int headMatches = countMatches(head, 0, (pageList.count() - 1) * cPageSize + patternSize - 1);
        emit matchFound(fileKey, headMatches);
        while (tail.count() < cPageCount) {
            tail.append(readNextPage(file, haveMoreData));
            if (!haveMoreData) break;
        }
		pageList.clear();
        pageList.append(tail);
    }
    int finalMatches = countMatches(pageList.join(), 0, fileSize);
    emit matchFound(fileKey, finalMatches);
    emit checkDone(fileKey, 1.0e-9l * timer.nsecsElapsed());
    cleanup();
}
