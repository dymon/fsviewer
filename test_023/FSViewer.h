#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "CurrDirViewer.h"
#include "SimpleTable.h"

#include <QMainWindow>
#include <QByteArrayMatcher>
#include <QFileSystemModel>
#include <QMutex>

namespace Ui {
class MainWindow;
}

class CurrDirViewer;
class FSViewer : public QMainWindow
{
    Q_OBJECT

    QString _hex, _prevHex;
    char hexDig(QChar qc);
public:
    explicit FSViewer(QWidget *parent = 0);
    ~FSViewer();
    Ui::MainWindow *ui;
    QFileSystemModel *fsModel;\
    CurrDirViewer *cdViewer;
    SimpleTableModel *tblModel;
    QByteArray *ppat;
    QByteArray &matchPattern() { return *ppat; }
    void setCurrentPathLabel(const QString &labText);
    void setSearchPatternLabel(const QString &labText);
    void setStatus(const QString &msgText);
    void resizeColumnsToContext();
public slots:
    void currentChanged(QModelIndex fsInx);
    void on_lineEdit_editingFinished();
    void on_lineEdit_textChanged(const QString &hex);
};

#endif // MAINWINDOW_H
