#include "QHexByteValidator.h"

bool QHexByteValidator::isLengthEven(const QString &str) const
{
    int len = str.length();
    return !(len & 1);
}

QHexByteValidator::QHexByteValidator()
{

}

QValidator::State QHexByteValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos);
    return isLengthEven(input)
            ? Acceptable
            : Intermediate;
}

void QHexByteValidator::fixup(QString &input) const
{
    if (!isLengthEven(input)) {
        input += "0";
    }

}

