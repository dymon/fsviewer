#ifndef QHEXBYTEVALIDATOR_H
#define QHEXBYTEVALIDATOR_H

#include <QValidator>

class QHexByteValidator : public QValidator
{
    bool isLengthEven(const QString &str) const;
public:
    QHexByteValidator();
    State validate(QString & input, int & pos) const;
    void fixup(QString & input) const;
};

#endif // QHEXBYTEVALIDATOR_H
