#ifndef CURRDIRVIEWER_H
#define CURRDIRVIEWER_H

#include "FSViewer.h"
#include "FileChecker.h"
#include <QDir>
#include <QtDebug>

class FSViewer;
class FileChecker;
class CurrDirViewer: public QDir
{
    QStringList fnameList, fsizeList, fcountList, ftimeList;
    QList<FileChecker*> fchkList;
    FSViewer *fsv;
public:
    CurrDirViewer(const FSViewer *fsv);
    ~CurrDirViewer();
    void startMonitor(QString &path, bool forceRestart = false);
    void forget(const QString &path);
};
class CurrDirViewer;

#endif // CURRDIRVIEWER_H
