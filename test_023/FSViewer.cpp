#include "FSViewer.h"
#include "GeneratedFiles/ui_mainwindow.h"

#include <QWidget>
#include <QDebug>

FSViewer::FSViewer(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , fsModel(new QFileSystemModel)
    , cdViewer(new CurrDirViewer(this))
{
	_prevHex = "0D0A";
    ppat = new QByteArray("\x0d\x0a");
    ui->setupUi(this);
    //Already connected by QMetaObject::connectSlotsByName(MainWindow);
    //connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(on_lineEdit_textChanged(QString)));
    //connect(ui->lineEdit, SIGNAL(editingFinished(void)), this, SLOT(on_lineEdit_editingFinished(void)));
    ui->lineEdit->setText("0D 0A");
    fsModel->setRootPath(QDir::rootPath());
    //fsModel->setRootPath("d:/projects/bitmart/example/");
    fsModel->setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
    connect(ui->treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(currentChanged(QModelIndex)));
    ui->treeView->setModel(fsModel);
    ui->treeView->header()->setStretchLastSection(false);
    // column 0: Name
    ui->treeView->setColumnWidth(0, 300);
    ui->treeView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    // column 1: Size
    ui->treeView->hideColumn(1);
    // column 2: Type
    ui->treeView->setColumnWidth(2, 50);
    ui->treeView->header()->setSectionResizeMode(2, QHeaderView::Interactive);
    // column 3: Date Modified
    ui->treeView->hideColumn(3);
    ui->tableView->setModel(tblModel);
    cdViewer->startMonitor(fsModel->rootPath());
    setCurrentPathLabel(fsModel->rootPath());
}

FSViewer::~FSViewer()
{
    delete cdViewer;
    delete tblModel;
    delete fsModel;
    delete ui;
}

void FSViewer::setCurrentPathLabel(const QString &labText)
{
    ui->currentPathLabel->setText(labText);
}

void FSViewer::setSearchPatternLabel(const QString &labText)
{
    ui->searchPatternLabel->setText(labText);
}

void FSViewer::setStatus(const QString &msgText)
{
    ui->statusBar->showMessage(msgText, 0);
}

void FSViewer::resizeColumnsToContext()
{
    ui->tableView->resizeColumnsToContents();
}

void FSViewer::currentChanged(QModelIndex fsInx)
{
    cdViewer->startMonitor(fsModel->filePath(fsInx));
    setCurrentPathLabel(fsModel->filePath(fsInx));
}

void FSViewer::on_lineEdit_editingFinished()
{
	if (_hex == _prevHex) return;
    setSearchPatternLabel(QString("Hex pattern %1 length %2 byte(s)").arg(_hex).arg(ppat->length()));
	_prevHex = _hex;
	QString dummy;
	cdViewer->startMonitor(dummy, true);
}

void FSViewer::on_lineEdit_textChanged(const QString &hex)
{
    _hex = hex.trimmed().replace(" ","");
    if (_hex.length() & 1) _hex += "0";
    ppat->clear();
    for (int i = 0; i < _hex.length(); i += 2) {
        char nb = hexDig(_hex.at(i)) * 16 + hexDig(_hex.at(i + 1));
        ppat->append(nb);
    }
}

char FSViewer::hexDig(QChar qc)
{
    // Assume that lowercase hex digits are excluded by QLineEdit Input Mask (see mainwindow.ui)
    char _q = qc.unicode();
    if ((qc >= QChar('A')) && (qc <= QChar('F'))) return 10 + _q - ('A');
    if ((qc >= QChar('0')) && (qc <= QChar('9'))) return _q - ('0');
    return 0x7F; // signal an error
}
