#ifndef FILECHECKER_H
#define FILECHECKER_H

#include "FSViewer.h"
#include <QObject>
#include <QByteArray>
#include <QByteArrayData>
#include <QByteArrayList>
#include <QByteArrayMatcher>
#include <QFile>
#include <QRunnable>
#include <QtDebug>

class FSViewer;
class FileChecker : public QObject, public QRunnable
{
    Q_OBJECT

    FSViewer *fsv;
    static int cPageSize;
    static int cPageCount;
    QByteArray readNextPage(QFile &file, bool &haveTail);
    QByteArrayList pageList;
    QByteArrayMatcher *matcher;
    int countMatches(const QByteArray &ba, int from, int to);

public:
    FileChecker(const FSViewer *_fsv);
    ~FileChecker();
    void startCheck(const QString &_fileName, qint64 _fileSize, int _fileKey);
    void run();
    void cleanup();

	QString fileName;
    qint64 fileSize, size;
    int fileKey, patternSize;

signals:
    void matchFound(int fileKey, int matchCount);
    void checkDone(int fileKey, double stdtime);
};

#endif // FILECHECKER_H
