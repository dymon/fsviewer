#-------------------------------------------------
#
# Project created by QtCreator 2016-02-19T14:10:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_023
TEMPLATE = app


SOURCES += main.cpp\
    FSViewer.cpp \
    CurrDirViewer.cpp \
    SimpleTable.cpp \
    FileChecker.cpp

HEADERS  += \
    FSViewer.h \
    CurrDirViewer.h \
    SimpleTable.h \
    FileChecker.h

FORMS    += mainwindow.ui
