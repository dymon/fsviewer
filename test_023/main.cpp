#include "FSViewer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FSViewer w;
    w.show();

    return a.exec();
}
