#include "CurrDirViewer.h"

#include <QThreadPool>

CurrDirViewer::CurrDirViewer(const FSViewer *_fsv)
    : QDir("", "*", QDir::Name, QDir::Files | QDir::Hidden | QDir::System)
{
    fsv = const_cast<FSViewer*>(_fsv);
    QStringList *clist[] = {&fnameList, &fsizeList, &fcountList, &ftimeList};
    QString ctitle[] = { "Name", "Size", "Match Count", "Search Time"};
    fsv->tblModel = new SimpleTableModel(4, (QStringList**)clist, (QString*)ctitle);
}

CurrDirViewer::~CurrDirViewer()
{
    fsv = 0;
}

void CurrDirViewer::startMonitor(QString &path, bool forceRestart)
{
	if (forceRestart) { // force restart is special mode for restart search in the current path when search pattern changed
		path = QDir::path();
	}
	else {
		if (!QDir::path().isEmpty()) {
			if (QDir::path() == path) {
				qDebug() << "Already Monitoring{Exit)->" << path;
				return;
			}
			forget(path);
		}
	}
    setPath(path);
    fnameList.clear();
    fsizeList.clear();
    fcountList.clear();
    ftimeList.clear();
    QThreadPool::globalInstance()->clear();
    QThreadPool::globalInstance()->waitForDone(500);
    fchkList.clear();
    for (int i = 0; i < entryInfoList().count(); i++) {
        fnameList.append(entryInfoList()[i].fileName());
        fsizeList.append(QString::number(entryInfoList()[i].size()));
        fcountList.append("0");
        ftimeList.append("?");
        fchkList.append(new FileChecker(fsv));
    }
    fsv->tblModel->adjustRows();
    fsv->setStatus(QString("Total files %1").arg(fnameList.count()));
    fsv->resizeColumnsToContext();
    qDebug() << "Start Monitoring->" << path << entryList().count() << entryList();
    for (int i = 0; i < fchkList.count(); i++) {
        fchkList[i]->startCheck(entryInfoList()[i].filePath(), entryInfoList()[i].size(), i);
        QThreadPool::globalInstance()->start(fchkList[i]);
    }
}

void CurrDirViewer::forget(const QString &path)
{
    if (QDir::path() == path) {
        qDebug() << "Stop Monitoring->" << path;
    }
}
